package at.itkollegimst.muernseer.pos1makro.test2.buchhandlung.domain.model.valueobjekts;

public enum Status {
    NONE, BESTELLT, ABHOLBEREIT;
}
