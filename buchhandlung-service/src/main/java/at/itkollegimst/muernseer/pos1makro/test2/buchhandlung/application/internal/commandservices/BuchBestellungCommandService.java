package at.itkollegimst.muernseer.pos1makro.test2.buchhandlung.application.internal.commandservices;

import at.itkollegimst.muernseer.pos1makro.test2.buchhandlung.domain.model.aggregates.BestellId;
import at.itkollegimst.muernseer.pos1makro.test2.buchhandlung.domain.model.aggregates.Bestellung;
import at.itkollegimst.muernseer.pos1makro.test2.buchhandlung.domain.model.commands.BuchBestelltCommand;
import at.itkollegimst.muernseer.pos1makro.test2.buchhandlung.domain.model.valueobjekts.Status;
import at.itkollegimst.muernseer.pos1makro.test2.buchhandlung.infastructure.repositories.jpa.BestellungRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class BuchBestellungCommandService {

    @Autowired
    private BestellungRepository bestellungRepository;

    public BestellId bestellen(BuchBestelltCommand buchBestelltCommand) {

        String random = UUID.randomUUID().toString().toUpperCase();
        buchBestelltCommand.setBestellid(random.substring(0, random.indexOf("-")));
        buchBestelltCommand.setStatus(Status.BESTELLT);
        Bestellung bestellung = new Bestellung(buchBestelltCommand);
        bestellungRepository.save(bestellung);
        return new BestellId(buchBestelltCommand.getBestellid());
    }

    public List<Bestellung> listAll() {
        return bestellungRepository.findAll();
    }
}
