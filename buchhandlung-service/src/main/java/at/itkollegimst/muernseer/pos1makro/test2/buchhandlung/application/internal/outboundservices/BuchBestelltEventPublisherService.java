package at.itkollegimst.muernseer.pos1makro.test2.buchhandlung.application.internal.outboundservices;

import at.itkollegimst.muernseer.pos1makro.test2.buchhandlung.infastructure.brokers.rabbitmq.BuchBestelltEventSource;
import at.itkollegimst.muernseer.pos1makro.test2.shareddomain.events.BuchBestelltEvent;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionalEventListener;

@Service
@EnableBinding(BuchBestelltEventSource.class)
public class BuchBestelltEventPublisherService {

    private BuchBestelltEventSource buchBestelltEventSource;

    public BuchBestelltEventPublisherService(BuchBestelltEventSource buchBestelltEventSource) {
        this.buchBestelltEventSource = buchBestelltEventSource;
    }

    @TransactionalEventListener
    public void handleBuchBestelltEvent(BuchBestelltEvent buchBestelltEvent) {
        buchBestelltEventSource.buchBestellung().send(MessageBuilder.withPayload(buchBestelltEvent).build());
    }
}
