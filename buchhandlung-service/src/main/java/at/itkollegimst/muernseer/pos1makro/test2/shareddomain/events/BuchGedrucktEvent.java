package at.itkollegimst.muernseer.pos1makro.test2.shareddomain.events;

public class BuchGedrucktEvent {

    private BuchGedrucktEventData buchGedrucktEventData;

    public BuchGedrucktEvent() {

    }

    public BuchGedrucktEvent(BuchGedrucktEventData buchGedrucktEventData) {
        this.buchGedrucktEventData = buchGedrucktEventData;
    }

    public BuchGedrucktEventData getBuchGedrucktEventData() {
        return buchGedrucktEventData;
    }

    public void setBuchBestelltEventData(BuchGedrucktEventData buchGedrucktEventData) {
        this.buchGedrucktEventData = buchGedrucktEventData;
    }
}
