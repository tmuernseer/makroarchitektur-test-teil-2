package at.itkollegimst.muernseer.pos1makro.test2.shareddomain.events;

public class BuchGedrucktEventData {

    private String bestellid;

    public BuchGedrucktEventData() {

    }

    public BuchGedrucktEventData(String bestellid) {
        this.bestellid = bestellid;
    }

    public String getBestellid() {
        return bestellid;
    }

    public void setBestellid(String bestellid) {
        this.bestellid = bestellid;
    }
}
