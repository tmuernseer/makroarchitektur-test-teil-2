package at.itkollegimst.muernseer.pos1makro.test2.buchhandlung.interfaces.rest;

import at.itkollegimst.muernseer.pos1makro.test2.buchhandlung.application.internal.commandservices.BuchBestellungCommandService;
import at.itkollegimst.muernseer.pos1makro.test2.buchhandlung.domain.model.aggregates.BestellId;
import at.itkollegimst.muernseer.pos1makro.test2.buchhandlung.domain.model.aggregates.Bestellung;
import at.itkollegimst.muernseer.pos1makro.test2.buchhandlung.domain.model.commands.BuchBestelltCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/buchbestellung")
public class BuchBestelltController {
    @Autowired
    private BuchBestellungCommandService buchBestellungCommandService;

    @GetMapping
    public List<Bestellung> getBestellung() {
        return buchBestellungCommandService.listAll();
    }

    @PostMapping
    @ResponseBody
    public BestellId bestellen() {
        BestellId id = buchBestellungCommandService.bestellen(new BuchBestelltCommand());
        System.out.println("*****BUCH BESTELLT*****" + id.getBestellid());
        return id;
    }

}
