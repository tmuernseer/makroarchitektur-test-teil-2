package at.itkollegimst.muernseer.pos1makro.test2.buchhandlung.domain.model.aggregates;

import at.itkollegimst.muernseer.pos1makro.test2.buchhandlung.domain.model.commands.BuchBestelltCommand;
import at.itkollegimst.muernseer.pos1makro.test2.shareddomain.events.BuchBestelltEvent;
import at.itkollegimst.muernseer.pos1makro.test2.buchhandlung.domain.model.valueobjekts.Status;
import at.itkollegimst.muernseer.pos1makro.test2.shareddomain.events.BuchBestelltEventData;
import org.springframework.data.domain.AbstractAggregateRoot;

import javax.persistence.*;

@Entity
@Table(name = "Bestellung")
public class Bestellung extends AbstractAggregateRoot<Bestellung> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idbestellung")
    private Long id;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;

    @Embedded
    private BestellId bestellId;

    public Bestellung() {

    }

    public Bestellung(BuchBestelltCommand command) {
        this.bestellId = new BestellId(command.getBestellid());
        addDomainEvent(new BuchBestelltEvent(new BuchBestelltEventData(bestellId.getBestellid())));
        this.status = command.getStatus();
    }

    private void addDomainEvent(Object event) {
        registerEvent(event);
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getBestellId() {
        return bestellId.getBestellid();
    }

    public void setBestellId(BestellId bestellId) {
        this.bestellId = bestellId;
    }
}
