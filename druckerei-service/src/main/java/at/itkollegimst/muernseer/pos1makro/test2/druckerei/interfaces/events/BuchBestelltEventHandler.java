package at.itkollegimst.muernseer.pos1makro.test2.druckerei.interfaces.events;

import at.itkollegimst.muernseer.pos1makro.test2.druckerei.application.internal.commandservices.BuchDruckCommandService;
import at.itkollegimst.muernseer.pos1makro.test2.druckerei.infastructure.brokers.rabbitmq.BuchBestelltEventBinding;
import at.itkollegimst.muernseer.pos1makro.test2.shareddomain.events.BuchBestelltEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Service;

@Service
@EnableBinding(BuchBestelltEventBinding.class)
public class BuchBestelltEventHandler {

    @Autowired
    private BuchDruckCommandService buchDruckCommandService;
    @StreamListener(target = BuchBestelltEventBinding.BESTELLUNG)
    public void reciveEvent(BuchBestelltEvent buchBestelltEvent){
        buchDruckCommandService.buchDrucken(buchBestelltEvent.getBuchBestelltEventData().getBestellid());
    }

}
