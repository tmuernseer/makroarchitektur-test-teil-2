package at.itkollegimst.muernseer.pos1makro.test2.shareddomain.events;

public class BuchBestelltEvent {

    private BuchBestelltEventData buchBestelltEventData;

    public BuchBestelltEvent() {

    }

    public BuchBestelltEvent(BuchBestelltEventData buchBestelltEventData) {
        this.buchBestelltEventData = buchBestelltEventData;
    }

    public BuchBestelltEventData getBuchBestelltEventData() {
        return buchBestelltEventData;
    }

    public void setBuchBestelltEventData(BuchBestelltEventData buchBestelltEventData) {
        this.buchBestelltEventData = buchBestelltEventData;
    }
}
