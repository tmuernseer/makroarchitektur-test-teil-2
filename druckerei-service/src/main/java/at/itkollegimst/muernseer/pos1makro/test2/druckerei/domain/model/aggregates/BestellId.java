package at.itkollegimst.muernseer.pos1makro.test2.druckerei.domain.model.aggregates;

import at.itkollegimst.muernseer.pos1makro.test2.shareddomain.events.BuchGedrucktEvent;
import at.itkollegimst.muernseer.pos1makro.test2.shareddomain.events.BuchGedrucktEventData;
import org.springframework.data.domain.AbstractAggregateRoot;

import javax.persistence.*;

@Entity
@Table(name = "bestellid")
public class BestellId extends AbstractAggregateRoot<BestellId> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "wert")
    private String wert;

    public BestellId() {

    }

    public BestellId(String wert) {
        this.wert = wert;
        addDomainEvent(new BuchGedrucktEvent(new BuchGedrucktEventData(wert)));
    }

    public void addDomainEvent(Object event) {
       registerEvent(event);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWert() {
        return wert;
    }

    public void setWert(String wert) {
        this.wert = wert;
    }
}
