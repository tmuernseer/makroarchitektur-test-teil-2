package at.itkollegimst.muernseer.pos1makro.test2.druckerei.infastructure.repositories.jpa;

import at.itkollegimst.muernseer.pos1makro.test2.druckerei.domain.model.aggregates.BestellId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BestellIdRepository extends JpaRepository<BestellId, Long> {

}
