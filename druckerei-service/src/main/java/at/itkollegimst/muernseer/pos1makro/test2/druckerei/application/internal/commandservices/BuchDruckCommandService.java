package at.itkollegimst.muernseer.pos1makro.test2.druckerei.application.internal.commandservices;

import at.itkollegimst.muernseer.pos1makro.test2.druckerei.domain.model.aggregates.BestellId;
import at.itkollegimst.muernseer.pos1makro.test2.druckerei.infastructure.repositories.jpa.BestellIdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class BuchDruckCommandService {

    @Autowired
    private BestellIdRepository bestellIdRepository;

    @Transactional
    public void buchDrucken(String id){

        BestellId bestellId = new BestellId(id);
        bestellIdRepository.save(bestellId);

        System.out.println("Buch aus Bestellung: " + bestellId.getWert() + " wird gedruckt!");

    }
}
