package at.itkollegimst.muernseer.pos1makro.test2.druckerei.application.internal.outboundservices;

import at.itkollegimst.muernseer.pos1makro.test2.druckerei.infastructure.brokers.rabbitmq.BuchBestelltEventBinding;
import at.itkollegimst.muernseer.pos1makro.test2.shareddomain.events.BuchGedrucktEvent;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionalEventListener;

@Service
@EnableBinding(BuchBestelltEventBinding.class)
public class BuchGedrucktEventPublisherService {

    private BuchBestelltEventBinding buchBestelltEventBinding;

    public BuchGedrucktEventPublisherService(BuchBestelltEventBinding buchBestelltEventBinding) {
        this.buchBestelltEventBinding = buchBestelltEventBinding;
    }

    @TransactionalEventListener
    public void handleBuchGedrucktEvent(BuchGedrucktEvent buchGedrucktEvent) {
        buchBestelltEventBinding.buchDruck().send(MessageBuilder.withPayload(buchGedrucktEvent).build());
    }
}

